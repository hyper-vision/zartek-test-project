"""luggage_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('luggage_app.urls')),
    path('api/token', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify', TokenVerifyView.as_view(), name='token_verify')
]


# """
# {
#     "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU4MTA0NzM1MSwianRpIjoiZjdhMWExZDNhZTUyNDgxZTkzZTE3YTI0NTA3YWE5MjYiLCJ1c2VyX2lkIjoxfQ.JFCpTldXl2yP5V3tjC6cD_3E3_twzWIdqfZ7a0wkHYI",
#     "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgwOTYxNzEyLCJqdGkiOiI1ZWYyYjA5YzcyYTg0NmUwYjU0ODYxMDQ4MjkzODUwZiIsInVzZXJfaWQiOjF9.Ib35Ef2WWQDfvd4d0XF5hxZ8n9fczgCmMbRC8S9TgP4"
# }
# """
