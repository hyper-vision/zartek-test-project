### Overview

---

#### Regular Users

- Regular users can make bookings via the `/bookings` endpoint.

- While booking a user can select from a list of items, each item has a predifined cost associated with it, which is determined by those with `POST` permission to the `Luggage` model.

- A user need not be logged in to make a booking.

- A user cannot access the `/luggage` or `/limits` endpoint without JWT authentication.

#### POST Users

- To access the `Luggage` model and make changes to it one must be authenticated using JWT authentication.

- JWT access and refresh tokens can be generated from `/api/token/` endpoint, the user must pass their username and password as parameters.

- JWT access tokens can be refreshed from `/api/token/refresh/` and verified at `/api/token/verify/`.

- Authenticated users can determine the limit of total luggage items as well as total bookings.

- The luggage and booking limit are defined in the `Limits` model which inherits from `SingletonModel` of the `django-solo` module. This allows us to define a model that can only have a max of one instance at any given time.

- The authenticated user can update the limits via `PUT` requests if an instance already exists or create a new set of limits if the model is empty. Attempting to create a second instance is disallowed.

#### UPDATE

---

- When a booking is made, it is checked against the booking and luggage limits defined in the `Limits` model.

- If the booking exceeds the booking limit, it is disallowed.

- If the booking exceeds the luggage limit, it is disallowed.

## Changes from TESTCOLAB
