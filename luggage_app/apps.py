from django.apps import AppConfig


class LuggageAppConfig(AppConfig):
    name = 'luggage_app'
