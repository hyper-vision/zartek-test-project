from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from .models import Luggage, Booking, Limits
from .serializers import LuggageSerializer, BookingSerializer, LimitsSerializer


# Create your views here.
class LimitsViewset(viewsets.ModelViewSet):
    queryset = Limits.objects.all()
    serializer_class = LimitsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    # Check if Limits is empty, if so allow posting, else disallow posting
    def create(self, request):
        if self.queryset.count() >= 1:
            return Response({"status": 403, "detail": "Only one instance of Limits allowed"}, status=403)
        else:
            self.data = request.data
            new_limits = Limits(
                item_limit=self.data['item_limit'], booking_limit=self.data['booking_limit'])
            new_limits.save()
            return Response({"status": 200, "detail": "New Limits set"}, status=200)


class LuggageViewset(viewsets.ModelViewSet):
    queryset = Luggage.objects.all()
    serializer_class = LuggageSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class BookingViewset(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

    def create(self, request):
        # Get current limit of booking and luggage from Limits model
        booking_limit = Limits.objects.values(
            'booking_limit')[0]['booking_limit']
        luggage_limit = Limits.objects.values(
            'item_limit')[0]['item_limit']

        # Get current booking count and number of luggage items in current booking
        current_booking_count = Booking.objects.all().count()
        current_luggage_count = len(request.data.getlist('luggage_type'))

        # If current booking is over the limit, disallow
        if current_booking_count >= booking_limit:
            return Response({
                "error": "Booking limit reache! Booking is now closed!"
            }, status=status.HTTP_400_BAD_REQUEST)

        # If current booking luggage count exceeds luggage limit, disallow
        if current_luggage_count > luggage_limit:
            return Response({
                "error": f"Luggage limit exceeded! You are only allowed maximum {luggage_limit} items."
            }, status=status.HTTP_400_BAD_REQUEST)

        # Else, save as usual
        serializer = BookingSerializer(
            data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
