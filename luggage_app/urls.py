from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

router.register("luggage", views.LuggageViewset)
router.register("bookings", views.BookingViewset)
router.register("limits", views.LimitsViewset)
urlpatterns = [
    path('', include(router.urls), name="home")
]
