from django.db import models
from solo.models import SingletonModel

# Create your models here.

# Models needed
# 1. Item (name and price)
# 2. Booking (source, dest, first name, last name, luggage_items[list] many2many with Item model)


class Limits(SingletonModel):
    item_limit = models.PositiveIntegerField(default=3)
    booking_limit = models.PositiveIntegerField(default=60)

    def __str__(self):
        return f"Site Limit {self.id}"

    class Meta:
        verbose_name = "Default Values"


class Luggage(models.Model):
    item_name = models.CharField(max_length=100)
    item_price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return f"{self.item_name} - {self.item_price}"


class Booking(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    luggage_type = models.ManyToManyField(
        Luggage)
    total_amount = models.DecimalField(
        max_digits=8, decimal_places=2, default=0.00)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
