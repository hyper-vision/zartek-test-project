# Generated by Django 3.0.2 on 2020-02-05 12:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('luggage_app', '0012_auto_20200205_0729'),
    ]

    operations = [
        migrations.CreateModel(
            name='Limits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_limit', models.IntegerField(default=3)),
                ('booking_limit', models.IntegerField(default=60)),
            ],
            options={
                'verbose_name': 'Limit of number of items allowed per person',
            },
        ),
        migrations.RenameModel(
            old_name='Item',
            new_name='Luggage',
        ),
        migrations.RenameField(
            model_name='booking',
            old_name='luggage_items',
            new_name='luggage_type',
        ),
    ]
