# Generated by Django 3.0.2 on 2020-02-04 09:52

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('luggage_app', '0003_booking'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='date_booked',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='date_of_travel',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
