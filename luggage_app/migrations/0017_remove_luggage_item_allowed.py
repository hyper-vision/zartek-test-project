# Generated by Django 3.0.2 on 2020-02-06 03:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('luggage_app', '0016_auto_20200205_1216'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='luggage',
            name='item_allowed',
        ),
    ]
