from rest_framework import serializers
from django.db.models import Sum  # for getting sum of item price
from .models import Luggage, Booking, Limits


class LimitsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Limits
        fields = ('item_limit', 'booking_limit')


class LuggageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Luggage
        fields = ('id', 'item_name', 'item_price')


class BookingSerializer(serializers.HyperlinkedModelSerializer):
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = Booking
        fields = ('id', 'url', 'first_name', 'last_name',
                  'luggage_type', 'total_price')

    def get_total_price(self, object):
        total = object.luggage_type.all().aggregate(Sum('item_price'))
        return total
